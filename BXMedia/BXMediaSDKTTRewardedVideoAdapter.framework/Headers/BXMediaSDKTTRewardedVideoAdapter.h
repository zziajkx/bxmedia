//
//  BXMediaSDKTTRewardedVideoAdapter.h
//  BXMediaSDKTTRewardedVideoAdapter
//
//  Created by likun on 2021/3/26.
//  Copyright © 2021 LIKUN. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for BXMediaSDKTTRewardedVideoAdapter.
FOUNDATION_EXPORT double BXMediaSDKTTRewardedVideoAdapterVersionNumber;

//! Project version string for BXMediaSDKTTRewardedVideoAdapter.
FOUNDATION_EXPORT const unsigned char BXMediaSDKTTRewardedVideoAdapterVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BXMediaSDKTTRewardedVideoAdapter/PublicHeader.h>

#import "BXMediaSDKTTRewardedVideoManager.h"
